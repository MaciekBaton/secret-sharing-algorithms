from cryptography.fernet import Fernet


class DataEncryptor:
    def __init__(self, key=None):
        if key:
            self.read_key(key)
        else:
            self.key = None

    def read_key(self, key_file: str):
        with open(key_file, 'rb') as file:
            self.key = file.read()

    def encrypt(self, data):
        if not self.key:
            raise RuntimeError("To encrypt data please specify key first.")

        encoded = str(data).encode()
        fernet = Fernet(self.key)
        encrypted = fernet.encrypt(encoded)
        return encrypted

    def decrypt(self, data):
        if not self.key:
            raise RuntimeError("To decrypt data please specify key first.")

        fernet = Fernet(self.key)
        return fernet.decrypt(data)
