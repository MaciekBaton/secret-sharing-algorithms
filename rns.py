from typing import List


def gen_primes(number: int):
    """ Generate an infinite sequence of prime numbers.
    """
    # Maps composites to primes witnessing their compositeness.
    # This is memory efficient, as the sieve is not "run forward"
    # indefinitely, but only as long as required by the current
    # number being tested.
    #
    D = {}

    # The running integer that's checked for primeness
    q = 2

    while q <= number:
        if q not in D:
            # q is a new prime.
            # Yield it and mark its first multiple that isn't
            # already marked in previous iterations
            #
            yield q
            D[q * q] = [q]
        else:
            # q is composite. D[q] is the list of primes that
            # divide it. Since we've reached q, we no longer
            # need it in the map, but we'll mark the next
            # multiples of its witnesses to prepare for larger
            # numbers
            #
            for p in D[q]:
                D.setdefault(p + q, []).append(p)
            del D[q]

        q += 1


def int2rns(secret: int):
    product = 1
    prime_factors = list(gen_primes(secret))
    results = []
    base = []
    for prime_factor in prime_factors:
        results.append(secret % prime_factor)
        base.append(prime_factor)
        product *= prime_factor
        if product > secret:
            break
    return results, base, product


def modulo_inverse(a, n):
    u = 1
    w = a
    x = 0
    z = n
    while w > 0:
        if w < z:
            q = u; u = x; x = q
            q = w; w = z; z = q
        q = w // z
        u -= q * x
        w -= q * z
    if z == 1:
        if x < 0:
            x += n
        return x


def reconstruct_rns(rns: List[int], base, product):
    alt_base = [product // base[i] for i in range(len(base))]
    residual_ones = [alt_base[i] * modulo_inverse(alt_base[i], base[i]) for i in range(len(base))]

    return sum([residual_ones[i] * rns[i] for i in range(len(base))]) % product
