from socket import *
from crypto import DataEncryptor

from cryptography.fernet import Fernet


class Receiver:
    def __init__(self, host, port, buffer_size):
        self.host = host
        self.port = port
        self.buffer_size = buffer_size

    def await_data(self):
        addr = (self.host, self.port)
        UDPSock = socket(AF_INET, SOCK_DGRAM)
        UDPSock.bind(addr)
        print("Waiting to receive messages...")
        while True:
            (data, addr) = UDPSock.recvfrom(self.buffer_size)
            print("Received message: " + str(data))
            break
        UDPSock.close()
        return data

    def read_key(self, key_file):
        with open(key_file, 'rb') as file:
            self.key = file.read()

    def decrypt(self, data):
        fernet = Fernet(self.key)
        return fernet.decrypt(data)


def main():
    receiver = Receiver(host='', port=13000, buffer_size=1024)
    data_encryptor = DataEncryptor(key="key.key")
    data = receiver.await_data()
    decrypted = data_encryptor.decrypt(data)
    print("Decoded message: " + decrypted.decode())
    with open("received_data.txt", "w") as file:
        file.write(decrypted.decode())


if __name__ == "__main__":
    main()
