from cryptography.fernet import Fernet


def generate_fernet_key(file_name: str):
    key = Fernet.generate_key()
    file = open(file_name, 'wb')
    file.write(key)
    file.close()


def main():
    generate_fernet_key('key.key')


if __name__ == "__main__":
    main()
