import argparse
import os
from ast import literal_eval
from socket import *

from crypto import DataEncryptor
from rns import int2rns, reconstruct_rns
from shamir import ShamirData, ShamirAlgorithmClass

parser = argparse.ArgumentParser()
parser.add_argument('--construct', help="Construct shares from secret", action='store_true')
parser.add_argument('--reconstruct', help='Reconstruct a secret', action='store_true')
parser.add_argument('--send', help='send shares to other user', action='store_true')


#data = [ShamirData(1234, 6, 3, 1613), ShamirData(420, 8, 3, 1613), ShamirData(2222, 10, 3, 2300)]


def shamir(secret: int):
    num_of_shares = int(input("Please specify number of shares: "))
    recovery_threshold = int(input("Please specify minimal number of shares to recover a secret: "))
    prime = int(input("Please specify prime number to create shares: "))
    data = ShamirData(secret, num_of_shares, recovery_threshold, prime)
    shamir_alg = ShamirAlgorithmClass(data)
    shares = shamir_alg.construct_shares()
    with open('shares.txt', 'w') as file:
        lines = [
            f"shamir; {num_of_shares}; {recovery_threshold}; {prime}\n",
            str(shares)
        ]
        file.writelines(lines)
        print("Shares saved to shares.txt file.")


def rns(secret: int):
    rns_result, base, product = int2rns(secret)
    with open('rns.txt', 'w') as file:
        lines = [
            f"rns; {base}; {product}\n",
            str(rns_result)
        ]
        file.writelines(lines)
        print("Results saved to rns.txt file.")


def send_message(encrypted_message, host, port):
    addr = (host, port)
    UDPSock = socket(AF_INET, SOCK_DGRAM)
    UDPSock.sendto(encrypted_message, addr)
    UDPSock.close()


def main():
    args = parser.parse_args()
    if args.construct:
        secret = input("Your secret number: ")
        while not secret.isdigit():
            secret = input("Secret must be a number: ")
        secret = int(secret)

        method = ""
        while method not in {"shamir", "rns"}:
            method = input("Which secret sharing method do you want to use(shamir/rns)? : ")
            if method == "shamir":
                shamir(secret)
            elif method == "rns":
                rns(secret)

    elif args.reconstruct:
        input_file = input("File with necessary data to reconstruct a secret: ")

        while not os.path.isfile(input_file):
            print(f"{input_file} is not a file.")
            input_file = input("File name: ")

        with open(input_file, 'r') as file:
            lines = file.readlines()
            if lines:
                config = list(map(lambda x: x.strip(), lines[0].split(';')))
                method = config[0]
                if method == "shamir":
                    num_of_shares, recovery_threshold, prime = config[1:]
                    shares = list(literal_eval(lines[1]))
                    secret = int(ShamirAlgorithmClass.reconstruct_secret(int(recovery_threshold), shares, int(prime)))
                    print(f"Reconstructed secret: {secret}")
                elif method == "rns":
                    base, product = config[1:]
                    base = list(literal_eval(base))
                    rns_result = list(literal_eval(lines[1]))
                    secret = reconstruct_rns(rns_result, base, int(product))
                    print(f"Reconstructed secret: {secret}")
                else:
                    raise RuntimeError(f"Unknown method: {method}.")
            else:
                raise RuntimeError("File is empty.")

    elif args.send:
        file = input("Which file do you want to send? :")
        while not os.path.isfile(file):
            print(f"{file} is not a file. Try again.")
            file = input("File name: ")

        with open(file, 'r') as f:
            data = f.read()

        print("Specify ip host and port that you want send message to.")
        ip = input("Ip host: ")
        port = int(input("Port: "))
        data_encryptor = DataEncryptor(key="key.key")
        encrypted = data_encryptor.encrypt(data)

        send_message(encrypted, ip, port)


if __name__ == "__main__":
    main()
