from collections import namedtuple
from functools import reduce

import numpy.polynomial.polynomial as polynomial
from numpy.polynomial.polynomial import Polynomial as Poly

ShamirData = namedtuple('ShamirData', ['secret', 'number_of_shares', 'recovery_threshold', 'prime'])


class ShamirAlgorithmClass:
    """class used for implementation of Shamir's secret sharing algorithm"""

    @staticmethod
    def create_lagrange_polynomial(j, x, k):
        """creation of Lagrange basis polynomial"""
        polys = []
        for m in range(k):
            if m != j:
                poly = Poly([-1 * x[m], 1]) // Poly([x[j] - x[m]])
                polys.append(poly)

        return reduce(lambda acc, prime: acc * prime, polys, 1)

    @staticmethod
    def create_linear_combination_lagrange_polynomial(x, y, k):
        """creation of linear combination of Lagrange basis polynomials"""
        return sum([y[j] * ShamirAlgorithmClass.create_lagrange_polynomial(j, x, k) for j in range(k)])

    def __init__(self, data: ShamirData):
        """
        Secret: secret
        number_of_shares: total number of shares
        k: recovery threshold
        prime: prime, where prime > Secret and prime > number_of_shares
        """

        self.Secret = data.secret
        self.number_of_shares = data.number_of_shares
        self.recovery_threshold = data.recovery_threshold
        self.prime = data.prime

        production_coefs = [data.secret, 166, 94]
        self.production_poly = Poly(production_coefs)

    def construct_shares(self):
        """used to generate shares in a production environment, based on a"""

        return [
            # We use % self.prime below to take advantage of finite field arithmetic
            (x, polynomial.polyval(x, self.production_poly.coef) % self.prime)
            for x in range(1, self.number_of_shares + 1)
        ]

    @staticmethod
    def reconstruct_secret(recovery_threshold, shares, prime):

        if len(shares) < recovery_threshold:
            raise Exception("Need more participants")

        x = [a for a, b in shares]
        y = [b for a, b in shares]

        # We use % self.prime below to take advantage of finite field arithmetic
        return ShamirAlgorithmClass.create_linear_combination_lagrange_polynomial(x, y, recovery_threshold).coef[
                   0] % prime
